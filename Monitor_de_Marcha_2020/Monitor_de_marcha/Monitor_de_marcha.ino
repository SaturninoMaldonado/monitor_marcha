#include <SoftwareSerial.h>

SoftwareSerial miBT(10,11);

int sensorPinA1 = A1;   // Sensor conectado a Analog 1
int sensorPinA2 = A2;   // Sensor conectado a Analog 2
int sensorPinA3 = A3;   // Sensor conectado a Analog 3
int sensorPinA4 = A4;   // Sensor conectado a Analog 4
int sensorPinA5 = A5;   // Sensor conectado a Analog 5
int sensorPinA6 = A6;   // Sensor conectado a Analog 6
int sensorPinA7 = A7;   // Sensor conectado a Analog 7

int resRead1;            // La Lectura de la Resistencia por División de Tensión
int resRead2;            // La Lectura de la Resistencia por División de Tensión
int resRead3;            // La Lectura de la Resistencia por División de Tensión
int resRead4;            // La Lectura de la Resistencia por División de Tensión
int resRead5;            // La Lectura de la Resistencia por División de Tensión
int resRead6;            // La Lectura de la Resistencia por División de Tensión
int resRead7;            // La Lectura de la Resistencia por División de Tensión
 
void setup(){
  Serial.begin(38400); // Enviaremos la información de depuración a través del Monitor de Serial
  miBT.begin(38400);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);
  pinMode(A6, INPUT);
  pinMode(A7, INPUT); 
  
}
 
void loop(){

  resRead1 = analogRead(sensorPinA1); // lectura del sensor (A1)
  resRead2 = analogRead(sensorPinA2); // lectura del sensor (A2)
  resRead3 = analogRead(sensorPinA3); // lectura del sensor (A3)
  resRead4 = analogRead(sensorPinA4); // lectura del sensor (A4)
  resRead5 = analogRead(sensorPinA5); // lectura del sensor (A5)
  resRead6 = analogRead(sensorPinA6); // lectura del sensor (A6)
  resRead7 = analogRead(sensorPinA7); // lectura del sensor (A7)
  
  //escribe el valor de la lectura del sensorer de fuerza
  miBT.print("Lectura sensores de fuerza  ");
  miBT.print(" ");
  miBT.print(resRead1);        
  miBT.print(" ");
  miBT.print(resRead2);        
  miBT.print(" ");
  miBT.print(resRead3);        
  miBT.print(" ");
  miBT.print(resRead4);        
  miBT.print(" ");
  miBT.print(resRead5);    
  miBT.print(" ");
  miBT.print(resRead6); 
  miBT.print(" ");
  miBT.println(resRead7);
  
/*   //escribe el valor de la lectura del sensorer de fuerza
  Serial.print("Lectura sensores de fuerza  ");
  Serial.print("A1 ");
  Serial.print(resRead1);
  Serial.print(" B1 ");
  Serial.print(resRead2);
  Serial.print(" C1 ");
  Serial.print(resRead3);
  Serial.print(" D1 ");
  Serial.print(resRead4);
  Serial.print(" E1 ");
  Serial.print(resRead5);
  Serial.print(" F1 ");
  Serial.print(resRead6);
  Serial.print(" G1 ");
  Serial.println(resRead7);
*/
}

#include <SoftwareSerial.h>

SoftwareSerial miBT(10,11);

//declaracion de variables y pines
int sensorPinA3 = A3;   // Sensor conectado a Analog 1
int sensorPinA4 = A4;   // Sensor conectado a Analog 2
int sensorPinA5 = A5;   // Sensor conectado a Analog 3
int sensorPinA6 = A6;   // Sensor conectado a Analog 4
int sensorPinA7 = A7;   // Sensor conectado a Analog 5
int resRead3;            // La Lectura de la Resistencia por División de Tensión
int resRead4;            // La Lectura de la Resistencia por División de Tensión
int resRead5;            // La Lectura de la Resistencia por División de Tensión
int resRead6;            // La Lectura de la Resistencia por División de Tensión
int resRead7;            // La Lectura de la Resistencia por División de Tensión
//float val1;
//float val2; 
//float val3;
//float val4;
//float val5;
 
void setup(){
  Serial.begin(38400); // Enviaremos la información de depuración a través del Monitor de Serial
  miBT.begin(38400);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);
  pinMode(A6, INPUT);
  pinMode(A7, INPUT); 
  
}
 
void loop(){
  
  resRead3 = analogRead(sensorPinA3); // lectura del sensor (A3)
  resRead4 = analogRead(sensorPinA4); // lectura del sensor (A4)
  resRead5 = analogRead(sensorPinA5); // lectura del sensor (A5)
  resRead6 = analogRead(sensorPinA6); // lectura del sensor (A6)
  resRead7 = analogRead(sensorPinA7); // lectura del sensor (A7)

//  val1=resRead1*(10000/1023);
//  val2=resRead2*(10000/1023);
//  val3=resRead3*(30000/1023);
//  val4=resRead4*(20000/1023);
//  val5=resRead5*(20000/1023);


  
  //escribe el valor de la lectura del sensorer de fuerza
 // miBT.print("Lectura sensores de fuerza  ");
 // miBT.print(" ");
 // miBT.print(resRead5);        
 // miBT.print(" ");
 // miBT.print(resRead3);        
 // miBT.print(" ");
 // miBT.print(resRead6);    
 // miBT.print(" ");
 // miBT.print(resRead4); 
 // miBT.print(" ");
 // miBT.println(resRead7);

  miBT.println(" " + String(resRead5) + " " + String(resRead3) + " " + String(resRead6) + " " + String(resRead4) + " " + String(resRead7)); 
  
 // delay(10);

   //escribe el valor de la lectura del sensorer de fuerza
//  Serial.print("Lectura sensores de fuerza  ");
//  Serial.print("A1 ");
//  Serial.print(val1);
//  Serial.print(" B1 ");
//  Serial.print(val2);
//  Serial.print(" C1 ");
//  Serial.print(val3);
//  Serial.print(" D1 ");
//  Serial.print(val4);
//  Serial.print(" E1 ");
//  Serial.println(val5);
//  delay(600);

}
